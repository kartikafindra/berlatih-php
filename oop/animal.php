<?php

class Animal {
    public $name;
function __construct($name ) {
    $this->name = $name; 
    $this->legs = 2; 
    $this->cold_blooded = "false";

  }

function get_name() {
    return $this->name;
  }
  function get_legs() {
    return $this->legs;
  }
  function get_cold_blooded() {
    return $this->cold_blooded;
  }
}

class Ape extends Animal{
        public function yell(){
            echo "AUUOO";
        }
}
class Frog extends Animal{
  function __construct($name) {
    $this->name = $name;
    $this->legs = 4;
  }
    public function jump(){
        echo "hophop";
    }
}

 // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>