<?php
require_once 'Animal.php';
$sheep = new Animal("Shaun");
echo $sheep->get_name(); // "shaun"
echo "</br>";
echo $sheep->get_legs(); // 2
echo "</br>";
echo $sheep->get_cold_blooded() ;
echo "<br>";
$sungokong = new Ape("kera sakti");
echo $sungokong->get_name() . "</br>";
echo $sungokong->get_legs();
echo "</br>";
$sungokong->yell() ;// "Auooo"
echo "<br>";
$kodok = new Frog("buduk");
echo $kodok->get_name() . "</br>";
echo $kodok->get_legs() . "</br>";
$kodok->jump() ; // "hop hop"

?>